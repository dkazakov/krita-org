# Krita.org website

Marketing website for the open source art application Krita. Built with the Hugo static site generator. Currently deployed at https://dev.krita.org

## Building and testing the site locally

This site is based on [hugo-kde theme](https://invent.kde.org/websites/hugo-kde/), which is the common theme for various KDE websites that are based on Hugo.

Here are the steps to build site on Ubuntu-based system (tested on Ubuntu 20.04):

```bash
# intall golang and npm
sudo apt install golang npm

# install this specific version of Hugo (which is used on KDE's CI)
# if in doubt, check the version here:
# https://invent.kde.org/sysadmin/ci-images/-/blob/master/staticweb/Dockerfile?ref_type=heads#L46

wget https://github.com/gohugoio/hugo/releases/download/v0.110.0/hugo_extended_0.110.0_linux-amd64.deb
sudo dpkg -i hugo_extended_0.110.0_linux-amd64.deb

# build the site and install
hugo mod npm pack
npm install
hugo
hugo server
```

In case of troubles follow [the KDE-wide Hugo building guide on its wiki](https://invent.kde.org/websites/hugo-kde/-/wikis/Getting-started).

If you see a white page, you might have to run this before the "hugo server" command

    hugo mod clean

This usually means there was an update to the hugo theme, and your cached version if messing it up.

## Development Notes

See the [DEVELOPMENT](/DEVELOPMENT.md) guide for more information about technical changes.

## Day-to-day tasks

See the [USAGE](/USAGE.md) guide for more information on creating posts, pages, and doing releases through the Web IDE.
