---
title: "Two New Episodes of Pepper & Carrot Motion Comic by the Morevna Project"
date: "2022-12-09"
---

The Morevna Project has released two episodes of the Pepper&Carrot Motion Comic. Most of the artwork, including some frame-by-frame animation scenes, is painted using Krita.


{{< youtube nuw1B334feI >}}


{{< youtube -wWIZ7wT8eg >}}

Pepper & Carrot is a webcomic created by David Revoy, who is known for his contributions to Krita and other libre art projects. The sources of the webcomic are available to everyone under a CC-BY license and the team of Morevna Project used them to create the animated series. They also publish all source files of their motion comic: you can find them [here](https://morevnaproject.org/2022/11/27/sources-of-peppercarrot-episodes-3-4-and-5/).
