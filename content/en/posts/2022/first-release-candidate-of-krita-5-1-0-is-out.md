---
title: "First Release Candidate of Krita 5.1.0 is out"
date: "2022-08-04"
categories: 
  - "development-builds"
  - "news"
---

Today we're releasing the first release candidate for Krita 5.1! For the full list, check out the work-in-progress full [release notes](en/release-notes/krita-5-1-release-notes/)! Or check out Wojtek Trybus' release video:

{{< youtube TnvCjziCUGI >}}
 

{{< support-krita-callout >}}

Warning: beta 1 had a bug where brush presets were saved incorrectly, leading to crashes when trying to load those presets in other versions of Krita. [Here is a Python script](https://invent.kde.org/tymond/brushes-metadata-fixer) that can fix those presets.

Since [the second beta](https://krita.org/en/item/krita-5-1-beta-2/), the following bugs have been fixed:

- Several issues with renaming existing and system tags for resources have resolved. [BUG:453831](https://bugs.kde.org/show_bug.cgi?id=453831)
- Python: make it possible to get all pattern resources.
- Fix a slowdown when attaching a dynamic sensor to the ration option in the brush preset editor. [BUG:456668](https://bugs.kde.org/show_bug.cgi?id=456668)
- Fix scaling of the colorspace selector when display scaling. [BUG:456929](https://bugs.kde.org/show_bug.cgi?id=456929)
- Fix a memory leak in the storyboard docker. [BUG:456998](https://bugs.kde.org/show_bug.cgi?id=456998)
- Fix a memory leak in the tile engine. [BUG:456998](https://bugs.kde.org/show_bug.cgi?id=457998)
- Make the Alpha mode the default generator for the Halftone filter.
- Improve handling of switching between exporting just the frames or just the video for an animation. [BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- Clean up files when exporting an animation to frames is canceled. [BUG:443105](https://bugs.kde.org/show_bug.cgi?id=443105)
- Fix the Elliptic are to Bezier curve function in SVG so certain files can be loaded without Krita running out of memory. [BUG:456922](https://bugs.kde.org/show_bug.cgi?id=456922), [BUG:439145](https://bugs.kde.org/show_bug.cgi?id=439145)
- Fix issues handling gradient fills for vector objects. [BUG:456807](https://bugs.kde.org/show_bug.cgi?id=456807)
- Make sure the mimetype selector Ok and Cancel buttons can be translated. [BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- Fix a crash when cloning a document through the scripting API. [BUG:457080](https://bugs.kde.org/show_bug.cgi?id=457080)
- Reset the file path when creating a copy of an existing image. [BUG:457081](https://bugs.kde.org/show_bug.cgi?id=457081)
- Fix a crash when copy-pasting a group with vector layers. [BUG:457154](https://bugs.kde.org/show_bug.cgi?id=457154)
- Fix saving a image with a text object to PSD. [BUG:455988](https://bugs.kde.org/show_bug.cgi?id=455988)
- Fix a crash when undoing creating a text shape. [BUG:457125](https://bugs.kde.org/show_bug.cgi?id=457125)
- Fix a crash when loading a thumbnail for a PSD file with embedded resources. [BUG:457123](https://bugs.kde.org/show_bug.cgi?id=456123)
- Improve the performance of creating a thumbnail from a PSD file for the recent files list. [BUG:456907](https://bugs.kde.org/show_bug.cgi?id=456907)
- Fix loading PSD files with embedded patterns.
- Fix undeleting tags that have been removed. [BUG:440337](https://bugs.kde.org/show_bug.cgi?id=440337)
- Make it possible to open some invalid PSD files. [BUG:444844](https://bugs.kde.org/show_bug.cgi?id=444844)
- Fix positioning of layers returned from the G'Mic plugin. [BUG:456950](https://bugs.kde.org/show_bug.cgi?id=456950)
- Fix loading JPEG-XL images created by Substance Designer. [BUG:456738](https://bugs.kde.org/show_bug.cgi?id=456738)
- Fix showing the alpha channel in the color picker tool options widget.
- Fix a crash when there is a corrupt in image on the clipboard. [BUG:456778](https://bugs.kde.org/show_bug.cgi?id=456778)
- Fix the positioning when pasting multiple reference images. [BUG:456382](https://bugs.kde.org/show_bug.cgi?id=456382)
- Fix Krita hanging when double-clicking too fast with the contiguous selection tool. [BUG:450577](https://bugs.kde.org/show_bug.cgi?id=450577)
- Fix updating the current palette when selecting a color with the color picker. [BUG:455203](https://bugs.kde.org/show_bug.cgi?id=455203)
- Improve the calculation of the brush speed sensor. [BUG:453401](https://bugs.kde.org/show_bug.cgi?id=453401)

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.1.0-RC1-setup.exe](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.1.0-RC1.zip](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-x64-5.1.0-RC1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.1.0-RC1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1-x86_64.appimage)

The separate gmic-qt appimage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.1.0-RC1.dmg](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.dmg)

### Android

Due to changes in Google's SDK requirements, we could not make 5.1.0-RC1 APK's this time. They will return in the final release.

### Source code

- [krita-5.1.0-RC1.tar.gz](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.gz)
- [krita-5.1.0-RC1.tar.xz](https://download.kde.org/unstable/krita/5.1.0-RC1/krita-5.1.0-RC1.tar.xz)
