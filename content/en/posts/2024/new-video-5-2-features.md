---
title: "New Video: Krita 5.2 Features!"
date: "2024-01-05"
---

Ramon Miranda has published a new video on the Krita channel: Krita 5.2 Features. Take a look and learn what’s new in our latest release:

{{< youtube t81PkQwf7NM >}}