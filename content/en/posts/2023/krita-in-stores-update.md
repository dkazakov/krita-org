---
title: "Krita in Stores - Update!"
date: "2023-10-18"
---

For quite some time now Krita has been available in various app stores: Steam, the Windows Store, the Epic Store, Google Play. While you can always download Krita for all platforms for free from krita.org (or tinker with the source code and build it yourself), Krita in stores have a price tag.

Earlier this year, because inflation was eating into our development capability, we raised Krita's store price to $14,99. Initially, that increased our income, but with inflation also also eating into everyone's spending ability, we've reduced Krita's price to $9,99. This'll come into effect over the next week as stores all have different approval procedures.

But in even better news, after months of gruelling fights with the MacOS sandbox system that is mandatory for MacOS App Store applications, and the Apple review process, we've released Krita on the MacOS App Store (note **not** the iPadOS store). Note that if you search for Krita, you have to scroll down a lot because many other art applications have used the Krita keyword to drive traffic to their apps, and they have been in the store for longer.

![](images/posts/2023/Screenshot-2023-10-18-at-15.57.46-1024x651.png)


Krita in the MacOS store.

It's more expensive than on the other stores because it is hugely expensive to develop for the MacOS App Store. You need a $100 developer subscription _per_ developer, extra hardware and a lot of time. So on the MacOS App Store, Krita is $14,99 -- we wanted to select $12,99, but if you do that, Apple makes if $14,99.

If you want to support Krita directly (stores take a huge cut)  [donating](https://krita.org/en/support-us/donations/) or joining the [dev fund](https://fund.krita.org)!

{{< support-krita-callout >}}
