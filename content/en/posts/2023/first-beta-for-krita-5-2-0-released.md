---
title: "First Beta for Krita 5.2.0 Released"
date: "2023-07-27"
categories: 
  - "development-builds"
---

Krita 5.2.0 is a major new release, and the first of the Krita 5.2 series of releases. Especially internally, there have been huge changes and improvements. While there are a lot of important user-visible changes that will make artists' workflow smoother, most work has been inside Krita, preparing Krita for the future.

{{< youtube DAkXhTdARuI >}}


The [release notes](en/release-notes/krita-5-2-release-notes/) will give you all the details, but here are some highlights:

- For animation, audio playback has been rewritten completely using the [MLT framework](https://www.mltframework.org/).
- Also for animation, video export has been made much easier: you no longer need to separately download FFmpeg, since [FFmpeg](http://ffmpeg.org/) now built into Krita.
- As the first part of a two-part rewrite of text handling in Krita, the text shape engine has been rewritten completely. This provides support for text-on-path, vertical text, wrapped text, text-in-shape, OpenType, emoji and more. **HOWEVER**: _this was step one_. Step two is rewriting the actual tool you use to work with text. If you want to access the new features, you will need to use **SVG source code editor** from the text tool.
- The transform tool can now transform all selected layers.
- There is a whole new color selector that supports wide-gamut color spaces.
- There have been many improvements to the JPEG-XL file format handling. **NOTE**: because of problems updating to the latest version of the JPEG-XL library on Windows, not all improvements are already available in Beta1.
- The brush engine settings backend, which stores all the settings, has been rewritten to be more robust and maintainable, as well as preparing it for a user interface redesign.

And, of course, there are countless bug fixes, smaller and larger workflow improvements and user interface polish.

## Download

### Windows

If you're using the portable zip files, just open the zip file in Explorer and drag the folder somewhere convenient, then double-click on the Krita icon in the folder. This will not impact an installed version of Krita, though it will share your settings and custom resources with your regular installed version of Krita. For reporting crashes, also get the debug symbols folder.

Note that we are not making 32 bits Windows builds anymore.

- 64 bits Windows Installer: [krita-x64-5.2.0-beta1-setup.exe](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-x64-5.2.0-beta1-setup.exe)
- Portable 64 bits Windows: [krita-x64-5.2.0-beta1.zip](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-x64-5.2.0-beta1.zip)
- [Debug symbols. (Unpack in the Krita installation folder)](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-x64-5.2.0-beta1-dbg.zip)

### Linux

- 64 bits Linux: [krita-5.2.0-beta1-x86\_64.appimage](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-5.2.0-beta1-x86_64.appimage)

The separate gmic-qt AppImage is no longer needed.

(If, for some reason, Firefox thinks it needs to load this as text: to download, right-click on the link.)

### macOS

Note: if you use macOS Sierra or High Sierra, please [check this video](https://www.youtube.com/watch?v=3py0kgq95Hk) to learn how to enable starting developer-signed binaries, instead of just Apple Store binaries.

- macOS disk image: [krita-5.2.0-beta1.dmg](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-5.2.0-beta1.dmg)

### Android

We consider Krita on ChromeOS as ready for production. Krita on Android is still **_beta_**. Krita is not available for Android phones, only for tablets, because the user interface requires a large screen.

- [64 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-x86_64-5.2.0-beta1-release-signed.apk)
- [32 bits Intel CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-x86-5.2.0-beta1-release-signed.apk)
- [64 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-arm64-v8a-5.2.0-beta1-release-signed.apk)
- [32 bits Arm CPU APK](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-armeabi-v7a-5.2.0-beta1-release-signed.apk)

### Source code

- [krita-5.2.0-beta1.tar.gz](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-5.2.0-beta1.tar.gz)
- [krita-5.2.0-beta1.tar.xz](https://download.kde.org/unstable/krita/5.2.0-beta1/krita-5.2.0-beta1.tar.xz)

### md5sum

For all downloads, visit [https://download.kde.org/unstable/krita/5.2.0-beta1/](https://download.kde.org/unstable/krita/5.2.0-beta1) and click on Details to get the hashes.

### Key

The Linux AppImage and the source .tar.gz and .tar.xz tarballs are signed. You can retrieve the public key [here](https://files.kde.org/krita/4DA79EDA231C852B). The signatures are [here](https://download.kde.org/unstable/krita/5.2.0-beta1/) (filenames ending in .sig).
