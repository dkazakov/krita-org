---
title: "Textured Brushes: a new video by Ramon Miranda"
date: "2023-08-02"
---

Ramon Miranda has just released a new video on Krita's Youtube channel. Learn all about textured brushes!

{{< youtube tEbf7WKPGm4 >}}

There are two texture patterns you can download to experiment with and follow the video with:

- [RM Marble 03.png](https://files.kde.org/krita/extras/RM%20Marble%2003.png)
- [RM Abstract shapes 02.png](https://files.kde.org/krita/extras/RM%20Abstract%20shapes%2002.png)
