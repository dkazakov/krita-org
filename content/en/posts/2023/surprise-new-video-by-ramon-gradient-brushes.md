---
title: "Surprise New Video by Ramon: Gradient Brushes"
date: "2023-05-31"
---

Because you asked for it, here a surprise video by Ramon Miranda introducing gradient brushes:
 
{{< youtube t6lOYEGgz5k >}}