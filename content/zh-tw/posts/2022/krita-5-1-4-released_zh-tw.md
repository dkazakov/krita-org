---
title: "發佈 Krita 5.1.4 修正版本"
date: "2022-12-15"
categories: 
  - "news_zh-tw"
  - "officialrelease_zh-tw"
---

今天我們發佈了 Krita 5.1.4 修正版本。如無意外這將會是 5.1 版本系列的最後一次更新。之後我們將開始更新 Krita 的相依程式庫。下個新版本將會是包含大量更動的 Krita 5.2 喔！

## 問題修正

（譯者按：由於內容眾多而人力資源有限，故此段落不作完整翻譯，保留英文原文。）

- Vector shapes not swapping the current fg/bg color ([BUG:461692](https://bugs.kde.org/show_bug.cgi?id=461692))
- Fix a crash when using "Paste into Active Layer" ([BUG:462223](https://bugs.kde.org/show_bug.cgi?id=462223))
- Layer Styles: label the Outer Glow page as Outer, not Inner Glow. ([BUG:462091](https://bugs.kde.org/show_bug.cgi?id=462091))
- Parse transfer characteristics from ICC profiles. Patch by Rasyuga, thanks! (BUG:45911)
- Fix handling ICC color primaries and white point detections. Patch by Rasyuga, thanks!
- Remove two obsolete actions from the action list in settings->configure Krita->shortcuts
- Fix some display artifacts when using fractional display scaling. (BUG:441216, 460577, 461912)
- Fix wraparound mode for non-pixel brush engines (BUG:460299)
- Fix visibility of the measure and gradient tools on a dark background
- Fix data loss when a transform tool is applied too quickly. (BUG:460557, 461109)
- Android: Disable changing the resource location
- Android: Disable the touch docker (some buttons are completely broken, and we're rewriting Krita's touch functionality). BUG:461634
- Android: disable New Window (Android does not do windows).
- Android: disable workspaces that create multiple windows (Android does not do windows).
- Android: make TIFF import and export work
- Android: remove the detach canvas action (Android does not do windows).
- TIFF: Fix inconsistent alpha and Photoshop-style layered tiff export checkboxes (BUG:462925)
- TIFF: Fix handling multipage files (BUG:461975)
- TIFF: Implement detection of the resolution's unit. (BUG:420932)
- EXR: Implement consistent GRAY and XYZ exporting (BUG:462799)
- AVIF: add the image/avif mimetype to the desktop file so external applications can know Krita can open these files. (BUG:462224)
- PSD: allow zero-sized resource blocks (BUG:461493, 450983)
- Python: fix creating a new image from Python. (BUG:462665)
- Python: fix updating the filename when using Document::saveAs.  (BUG:462667)
- Python: make it possible to use Python 3.11 (BUG:461598)
- Animation: Improve the autokey functionality. (BUG:459723)

## 下載

### Windows

如果您使用免安裝版：請注意，免安裝版仍然會與安裝版本共用設定檔及資源。如希望以免安裝版測試並回報程式強制終止的問題，請同時下載偵錯符號 (debug symbols)。

注意：我們已不再提供為 32 位元 Windows 建置的版本。

- 64 位元安裝程式：[krita-x64-5.1.4-setup.exe](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-setup.exe)
- 64 位元免安裝版：[krita-x64-5.1.4.zip](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4.zip)
- [偵錯符號（請解壓到 Krita 程式資料夾之內）](https://download.kde.org/stable/krita/5.1.4/krita-x64-5.1.4-dbg.zip)

### Linux

- 64 位元 Linux：[krita-5.1.4-x86\_64.appimage](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4-x86_64.appimage)

Linux 版本現不再需要另行下載 G'Mic-Qt 外掛程式 AppImage。

### macOS

注意：如果您正在使用 macOS Sierra 或 High Sierra，請參見[這部影片](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何執行由開發者簽署的程式。

- macOS 軟體包：[krita-5.1.4.dmg](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.dmg)

### Android

我們仍視 ChomeOS 及 Android 的版本為**測試版本**。此版本或可能含有大量程式錯誤，而且仍有部份功能未能正常運作。由於使用者介面並未完善，軟體或須配合實體鍵盤才能使用全部功能。Krita 不適用於 Android 智慧型手機，只適用於平板電腦，因為其使用者介面設計並未為細小的螢幕作最佳化。

- [64 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-x86_64-5.1.4-release-signed.apk)
- [32 位元 Intel CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-x86-5.1.4-release-signed.apk)
- [64 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-arm64-v8a-5.1.4-release-signed.apk)
- [32 位元 Arm CPU APK](https://download.kde.org/stable/krita/5.1.4/krita-armeabi-v7a-5.1.4-release-signed.apk)

### 原始碼

- [krita-5.1.4.tar.gz](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.gz)
- [krita-5.1.4.tar.xz](https://download.kde.org/stable/krita/5.1.4/krita-5.1.4.tar.xz)

### 檔案校對碼 (md5sum)

請瀏覧 [https://download.kde.org/stable/krita/5.1.4/](https://download.kde.org/stable/krita/5.1.4) 並點選各下載檔案的「Details」連結以查閱該檔案的 MD5 / SHA1 / SHA256 校對碼。

### 數位簽章

Linux AppImage 以及原始碼的 .tar.gz 和 .tar.xz 壓縮檔已使用數位簽章簽名。您可以從[這裡](https://files.kde.org/krita/4DA79EDA231C852B)取得 GPG 公鑰。簽名檔可於[此處](https://download.kde.org/stable/krita/5.1.4/)找到（副檔名為 .sig）。
