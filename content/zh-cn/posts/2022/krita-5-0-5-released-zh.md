---
title: "Krita 5.0.5 正式版已经推出"
date: "2022-04-14"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.0.5 版。5.0.3 版和 5.0.4 版并没有真正发布，这两个版本号只是被用在了某些软件商店平台的 Krita 软件包更新上了——这些平台要求每次上传新版软件包都要刷新版本号。这可能是 Krita 5.0.x 系列的的最后一版，我们希望能在今年六月份发布 Krita 5.1.0。

此版软件包括下列更新：

- 修复了在 CMYK 图像上使用颜色涂抹笔刷会造成杂色的问题。 [BUG:447211](https://bugs.kde.org/show_bug.cgi?id=447211)
- 修复了四点透视变形时结果会变模糊的问题。 [BUG:447255](https://bugs.kde.org/show_bug.cgi?id=447255)
- 修复了液化变形属性的撤销问题。 [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- 修复了关于 Krita 对话框的大小。
- 修复了更改笔刷预设的快速预渲染设置会造成崩溃的问题。
- 修复了使用 G'Mic 插件时使用图层名称的问题。 [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293), [BUG:429851](https://bugs.kde.org/show_bug.cgi?id=429851)
- 为 G'Mic 滤镜添加了缺失的混合模式。 [BUG:447293](https://bugs.kde.org/show_bug.cgi?id=447293)
- 修复了资源库处理大写文件扩展名的问题。 [BUG:447454](https://bugs.kde.org/show_bug.cgi?id=447454)
- 在 macOS 上默认启用操作系统原生文件对话框。
- 修复 16 位整数通道图像在 Arm64 架构的 macOS 下的渲染错误。
- 修复在撤销多次图层操作过快时的崩溃问题。 [BUG:447462](https://bugs.kde.org/show_bug.cgi?id=447462)
- 临时应对当变形蒙版应用到设为穿透模式的图层组时的崩溃问题。 [BUG:447506](https://bugs.kde.org/show_bug.cgi?id=447506)
- 修复 Photoshop 兼容快捷键。 [BUG:447771](https://bugs.kde.org/show_bug.cgi?id=447771)
- 修复 AppimageUpdate 功能。 [BUG:446757](https://bugs.kde.org/show_bug.cgi?id=446757)
- 在图像属性对话框中正确显示图层数量。感谢 Dan McCarthy 提供补丁。
- 修复面板标题的布局。
- 禁用自动分配快捷键到所选的面板。
- 修复颜色历史显示中的移除内存泄漏问题。
- 修复元数据系统初始化时的一个 race 状态。
- 修复动画图像显示多个视图时的动画播放。 [BUG:450425](https://bugs.kde.org/buglist.cgi?quicksearch=450425)
- 修复带有动画变形蒙版的图像在调整图像大小时的数据丢失。 [BUG:450781](https://bugs.kde.org/show_bug.cgi?id=450781)
- 修复动画的文件路径或文件名更改时造成文件基名错误的问题。 [BUG:451654](https://bugs.kde.org/show_bug.cgi?id=451654)
- 修复画布输入设置的重置默认值功能。 [BUG:396064](https://bugs.kde.org/show_bug.cgi?id=396064)
- 在图层样式中正确更新渐变色。 [BUG:452066](https://bugs.kde.org/show_bug.cgi?id=452066)
- 修复处理触摸事件时的一个崩溃问题。
- 修复由 Coverity 发现的大量代码漏洞。
- 修复在高分辨率屏幕下曲线控件的点击框太小的问题。 [BUG:446755](https://bugs.kde.org/show_bug.cgi?id=446755)
- 修复了保存到已禁用资源的功能。
- 修复了多功能拾色器的 hsySH 方形元素。 [BUG:452422](https://bugs.kde.org/show_bug.cgi?id=452422)
- 修复了在高分辨率屏幕下的光标过小的问题。 [BUG:448107](https://bugs.kde.org/show_bug.cgi?id=448107), [BUG:447314](https://bugs.kde.org/show_bug.cgi?id=447314)
- 修复了透明度动画和滤镜蒙版动画的一些相关问题。 [BUG:452170](https://bugs.kde.org/show_bug.cgi?id=452170)
- 为动画矢量图层正确加载不透明度。 [BUG:452144](https://bugs.kde.org/show_bug.cgi?id=452144)
- 阻止分镜头脚本面板在已锁定的图层上创建帧。 [BUG:447396](https://bugs.kde.org/show_bug.cgi?id=447396)
- 分镜头脚本面板：添加复制已有场景副本的功能。
- 改进在输入动画或录像时与 FFmpeg 的交互过程。
- 改进导入损坏视频序列为动画时的检测功能。
- 临时应对在复制蒙版时无法复制全部像素的问题。 [BUG:453164](https://bugs.kde.org/show_bug.cgi?id=453164)
- 使保存和加载非 UTF-8 区域设置的渐变成为可能。 [BUG:447730](https://bugs.kde.org/show_bug.cgi?id=447730)
- 修复了更改色彩空间后蒙版和图层损坏的问题。
- 升级 LittleCMS 程序库到 2.13.1 版，修复一个灰阶色彩空间的程序缺陷，并修复了 8 位和 16 位灰阶图像互转时的问题。 [BUG:447484](https://bugs.kde.org/show_bug.cgi?id=447484)
- 修复自动笔尖带随机度时的延迟问题。 [BUG:446663](https://bugs.kde.org/show_bug.cgi?id=446663)
- 改善移动图层组中图层时的性能。 [BUG:450957](https://bugs.kde.org/show_bug.cgi?id=450957)
- 修复像素画笔刷的笔刷轮廓精度。 [BUG:447466](https://bugs.kde.org/show_bug.cgi?id=447466)
- 改进低密度笔刷的笔刷轮廓。 [BUG:447045](https://bugs.kde.org/show_bug.cgi?id=447045)
- 为 APNG 文件设置 .apng 扩展名而不是 .png。 [BUG:451473](https://bugs.kde.org/show_bug.cgi?id=451473)
- 修复在 HDR 图像中使用智能修复工具时会造成崩溃的问题。 [BUG:451912](https://bugs.kde.org/show_bug.cgi?id=451912)
- 使笔刷平滑高级模式的距离可设置为 100 以上。 [BUG:451874](https://bugs.kde.org/show_bug.cgi?id=451874)
- 修复相连颜色选择工具的颜色容差设置。 [BUG:447524](https://bugs.kde.org/show_bug.cgi?id=447524)
- 修复默认橡皮擦预设。 [BUG:447650](https://bugs.kde.org/show_bug.cgi?id=447650)
- 为下列滤镜添加缺失的快捷键：斜率/偏移/指数、曲线 - 跨通道颜色调整、半调、高斯高通、高度贴图转法线贴图、渐变映射、归一化、色板化。 [BUG:451337](https://bugs.kde.org/show_bug.cgi?id=451337)
- 修复保存新工作区的功能。 [BUG:446985](https://bugs.kde.org/show_bug.cgi?id=446985)
- 正确处理 ICC 色彩特性文件黑名单。
- 修复当 PSD 文件中包含大小为零的数据块时的“Photoshop 签名验证失败”警告消息。 [BUG:450983](https://bugs.kde.org/show_bug.cgi?id=450983)
- 使 Krita 在配置文件损坏时表现更加可靠。 [BUG:449983](https://bugs.kde.org/show_bug.cgi?id=449983)
- 添加压力和旋转的触摸支持。
- 修复安卓版本的翻译问题。 [BUG:448343](https://bugs.kde.org/show_bug.cgi?id=448343)
- 修复安卓版本的自动保存功能。
- 正确处理矢量图层的 SVG 路径的标记。 [BUG:447417](https://bugs.kde.org/show_bug.cgi?id=447417)
- 修复所选渐变不再图层样式对话框的渐变选择控件中显示时的崩溃现象，同时修复图案的类似问题。 [BUG:448296](https://bugs.kde.org/show_bug.cgi?id=448296), [BUG:445922](https://bugs.kde.org/show_bug.cgi?id=445922)
- 修复笔刷预设选择器中没有选择笔刷预设时的问题。 [BUG:449226](https://bugs.kde.org/show_bug.cgi?id=449226), [BUG:450121](https://bugs.kde.org/show_bug.cgi?id=450121)
- 修复 Qt 无障碍辅助功能处理的一个程序缺陷。 [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- 修复在使用高分辨率屏幕时多文档模式下最大化子窗口后的菜单栏高度问题。 [BUG:449118](https://bugs.kde.org/show_bug.cgi?id=449118)
- 修复折线工具的速度传感器。 [BUG:434488](https://bugs.kde.org/show_bug.cgi?id=434488)
- 修复在应用雨滴滤镜到空图层时导致崩溃的问题。 [BUG:449408](https://bugs.kde.org/show_bug.cgi?id=449408)
- 修复全部选择时的一个 race 状态。 [BUG:449122](https://bugs.kde.org/show_bug.cgi?id=449122)
- 改善绘画时的线程处理，从而改善相关性能和能耗效率。 [BUG:367901](https://bugs.kde.org/show_bug.cgi?id=367901), [BUG:360677](https://bugs.kde.org/show_bug.cgi?id=360677)
- 改善资源选择器在安装大量资源库厚的性能。
- 修复更新数据库结构时保存标签的问题。
- 使同时分配和取消多个资源的标签成为可能。
- 每一笔后均重置绘画辅助尺。 [BUG:448187](https://bugs.kde.org/show_bug.cgi?id=448187)
- 修复颜色涂抹半径范围。 [BUG:441682](https://bugs.kde.org/show_bug.cgi?id=441682)
- 导出资源文件失败后移除损坏的资源文件。 [BUG:446279](https://bugs.kde.org/show_bug.cgi?id=446279)
- 修复在高分辨率屏幕下调整图像大小后参考图像的更新异常。 [BUG:430988](https://bugs.kde.org/show_bug.cgi?id=430988)
- 修复 MyPaint 笔刷引擎预设的减慢跟踪参数与防抖功能配合使用时的问题。 [BUG:447562](https://bugs.kde.org/show_bug.cgi?id=447562)
- 修复在 HDR 模式下绘制画面的色带问题。 [BUG:445672](https://bugs.kde.org/show_bug.cgi?id=445672)
- 修复绘制 SVG 文件图层。 [BUG:448256](https://bugs.kde.org/show_bug.cgi?id=448256)
- 修复在 OpenGL 2.1 下面使用小型拾色器。 [BUG:447868](https://bugs.kde.org/show_bug.cgi?id=447868)
- 修复在没有配置主题时使用默认主题。 [BUG:448483](https://bugs.kde.org/show_bug.cgi?id=448483)
- 修复非原生文件对话框的预览图像的大小调整问题。 [BUG:447805](https://bugs.kde.org/show_bug.cgi?id=447805)
- 改进 Ctrl 修饰键的处理。 [BUG:438784](https://bugs.kde.org/show_bug.cgi?id=438784)
- 修复切换笔刷预设或工具时笔刷轮廓的更新问题。 [BUG:428988](https://bugs.kde.org/show_bug.cgi?id=428988), [BUG:442343](https://bugs.kde.org/show_bug.cgi?id=442343)
- 修复最近文件列表中的缩略图处理。
- 修复通过脚本编程 API 对填充图层的配置进行设置的功能。 [BUG:447807](https://bugs.kde.org/show_bug.cgi?id=447807)

![](images/2021-11-16_kiki-piggy-bank_krita5.png) Krita 是一个自由开源的软件项目，如果条件允许，请考虑通过[加入开发基金](https://fund.krita.org/)、[捐款](https://krita.org/en/support-us/donations/)或[购买视频教程](https://krita.org/en/shop/)等方式为我们提供资金支持。您的支持可以确保 Krita 的核心开发团队成员能够为 Krita 全职工作。

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- 插件翻译：G'MIC 插件[需要下载翻译包](https://share.weiyun.com/SBopNjOn)
- 网盘下载：请在官网下载困难时使用，更新时间可能滞后。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-x64-5.0.5-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-x86_64-5.0.5-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-x86-5.0.5-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-arm64-v8a-5.0.5-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.0.5/krita-armeabi-v7a-5.0.5-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 现在已经能够在 ChromeOS 下正常运行，但其他安卓系统的支持目前尚处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭小细长的设备进行优化，也尚未开发平板专属的模式，因此建议搭配键盘使用。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。
- 比较新款的安卓平板一般使用 ARM CPU，Chromebook 等一般使用 Intel CPU。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.0.5/krita-5.0.5.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- [ms5sum 校验码文本文件](https://download.kde.org/stable/krita/5.0.5/md5sum.txt)

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.0.5/)。
