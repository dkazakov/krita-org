---
title: "Krita 5.1.1 正式版已经推出"
date: "2022-09-14"
categories: 
  - "news-zh"
  - "officialrelease-zh"
---

今天我们为大家带来了 Krita 5.1.1 正式版。此版软件修复了一些已知的程序缺陷，包括某些用户会遇到启动时间耗时过长、复制矢量图层时会发生崩溃等问题。

除上述两个缺陷外，本版软件还修复了以下问题：

- 原生 macOS 触摸板手势现在可以正常工作。[BUG:456446](https://bugs.kde.org/show_bug.cgi?id=456446)
- 修复了安卓版本由于交换文件不被删除而导致的应用体积增大问题。
- 修复了安卓版本在启动时可能发生的崩溃。[BUG:458907](https://bugs.kde.org/show_bug.cgi?id=458907)
- 修复了数个与 MyPaint 笔刷引擎有关的程序缺陷。MyPaint 橡皮擦现在会调用正确的笔刷混合模式，在选择了一个 MyPaint 笔刷时，不支持的混合模式将被禁用。[BUG:453054](https://bugs.kde.org/show_bug.cgi?id=453054), [BUG:445206](https://bugs.kde.org/show_bug.cgi?id=445206)
- 修复了将动画导出为图像序列时的“序列起点编号”功能。[BUG:458997](https://bugs.kde.org/show_bug.cgi?id=458997)
- 修复了用户同时删除安装文件夹和运行时文件夹中的 kritadefault.profile 画布输入设置文件时会导致崩溃的问题。
- 读取 ACO 色板时，正确读取色块名称并显示到色板中。[BUG:458209](https://bugs.kde.org/show_bug.cgi?id=458209)
- 修复了在图层面板中选择一个图层时会造成的崩溃。[BUG:458546](https://bugs.kde.org/show_bug.cgi?id=458546)
- 改进硬度、宽高比、相似颜色选区工具的色差阈值等项的滑动条级数。
- 移动可绘制的图层节点时，只更新一次画布。
- 修复在所有平台下当 OpenGL 加速的画布上显示多个辅助尺时会显示黑色矩形的问题。[BUG:401940](https://bugs.kde.org/show_bug.cgi?id=401940)
- 改善读取使用 ZIP 算法压缩的图像的性能，如 .kra 和 .ora 文件。
- 修复打开单个图层的 PSD 文件。[BUG:458556](https://bugs.kde.org/show_bug.cgi?id=458556)
- 欢迎画面控件中的更新链接现在可以点击。[BUG:458034](https://bugs.kde.org/show_bug.cgi?id=458034)
- JPEG-XL：修复线性 HDR 导出和 16 位浮点导入。[BUG:458054](https://bugs.kde.org/show_bug.cgi?id=458054)

## 下载

### 中文版信息

- 中文支持：Krita 的所有软件包均内建中文支持，首次安装时会自动设置为操作系统的语言。
- 手动设置：菜单栏 --> Settings --> Switch Application Language (倒数第二项) --> 下拉选单 --> 中文 (底部)，重启程序生效。
- G'MIC 滤镜中文翻译：打开 G'MIC 滤镜 -> 设置 (左下角) -> 勾选“Translate Filters”。

### Windows 版本

- **64 位 Windows 安装程序** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-setup.exe) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位 Windows 免安装包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)
- **64 位程序崩溃调试包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-x64-5.1.1-dbg.zip) | [网盘下载](https://share.weiyun.com/aVyf2PXQ)

- **配套网盘资源 (中文社区维护)** [中文离线文档](https://share.weiyun.com/Dea2uj0M) | [FFmpeg 软件包](https://share.weiyun.com/6tH13bVC) | [G'Mic 滤镜汉化](https://share.weiyun.com/SBopNjOn) | [Krita 配置文件和资源清理工具](https://share.weiyun.com/SCCloC47)

- 32 位支持：最后一版支持 32 位 Windows 的 Krita 为 4.4.3，[本站下载](https://download.kde.org/stable/krita/4.4.3/krita-x86-4.4.3-setup.exe) | [网盘下载](https://share.weiyun.com/wdMnx1WB)。
- 免安装包：解压到任意位置，运行目录中的 Krita 快捷方式。不带文件管理器缩略图插件，与已安装版本共用配置文件和资源，但程序本身相互独立。
- 程序崩溃调试包：解压到 Krita 的安装目录，在报告程序崩溃问题时用于获取回溯追踪数据。日常使用无需下载此包。

### Linux 版本

- **64 位 Linux AppImage 程序包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1-x86_64.appimage) | [网盘下载](https://share.weiyun.com/j7Vrjx2m)
- G'Mic 插件已经整合到主程序包中
- 如果浏览器把链接作为文本打开，请右键点击链接另存为文件。

### macOS 版本

- **macOS 程序映像** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.dmg) | [网盘下载](https://share.weiyun.com/jc82ykle)

- 如果您还在使用 OSX Sierra 或者 High Sierra，请[观看此视频](https://www.youtube.com/watch?v=3py0kgq95Hk)了解如何启动由开发人员签名的可执行软件包。

### 安卓版本

- **64 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-x86_64-5.1.1-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **32 位 Intel CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-x86-5.1.1-release-signed.apk) | [网盘下载](https://share.weiyun.com/KXRP4Ec0)
- **64 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-arm64-v8a-5.1.1-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- **32 位 ARM CPU APK 安装包** [本站下载](https://download.kde.org/stable/krita/5.1.1/krita-armeabi-v7a-5.1.1-release-signed.apk) | [网盘下载](https://share.weiyun.com/AxnO4CZZ)
- Krita 在 ChromeOS 下已经可以稳定用于生产用途，但在一般安卓系统下仍处于测试阶段。
- 安卓版的整体功能与桌面版本几乎完全相同，它尚未对手机等屏幕狭长的设备进行优化，也尚未开发平板专属的模式。建议搭配键盘使用。
- 近期上市的安卓平板一般采用 ARM CPU，Chromebook 等上网本一般采用 Intel CPU。
- 请先尝试 64 位的两种安装包，不行的话再尝试 32 位的两种。

### 源代码

- [TAR.GZ 格式源代码包](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.gz)
- [TAR.XZ 格式源代码包](https://download.kde.org/stable/krita/5.1.1/krita-5.1.1.tar.xz)

### md5sum 校验码

适用于上述所有软件包，用于校验下载文件的完整性。如果您不了解文件校验，忽略即可：

- 请访问[下载目录页面](https://download.kde.org/stable/krita/5.1.1)，点击最右列的“Details”获取哈希值。

### 文件完整性验证密钥

Linux 的 Appimage 可执行文件包和源代码的 .tar.gz 和 .tar.xz tarballs 压缩包已经经过数字签名。您可以[在此下载公钥](https://files.kde.org/krita/4DA79EDA231C852B)，还可以在此下载[数字签名的 SIG 文件](https://download.kde.org/stable/krita/5.1.1/)。
