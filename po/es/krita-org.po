#
# SPDX-FileCopyrightText: 2022, 2023, 2024 Eloy Cuadra <ecuadra@eloihr.net>
# Sofía Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: krita-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-31 01:59+0000\n"
"PO-Revision-Date: 2024-02-24 21:10+0100\n"
"Last-Translator: Sofía Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"SPDX-FileCopyrightText: FULL NAME <EMAIL@ADDRESS>\n"
"SPDX-License-Identifier: LGPL-3.0-or-later\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.08.4\n"

#: config.yaml:0
msgid "Krita"
msgstr "Krita"

#: config.yaml:0
msgid ""
"Krita is a professional FREE and open source painting program. It is made by "
"artists that want to see affordable art tools for everyone."
msgstr ""
"Krita es un programa de pintura digital profesional, LIBRE, gratuito y de "
"código abierto. Está creado por artistas que quieren ver herramientas de "
"arte asequibles para todos."

#: config.yaml:0
msgid "Learn"
msgstr "Aprender"

#: i18n/en.yaml:0
msgid "Recent Posts"
msgstr "Publicaciones recientes"

#: i18n/en.yaml:0
msgid "Donate"
msgstr "Hacer un donativo"

#: i18n/en.yaml:0
msgid "Software"
msgstr "Software"

#: i18n/en.yaml:0
msgid "Education"
msgstr "Educativo"

#: i18n/en.yaml:0
msgid "Foundation"
msgstr "Fundación"

#: i18n/en.yaml:0
msgid "Language"
msgstr "Idioma"

#: i18n/en.yaml:0
msgid "Report a bug"
msgstr "Informar de fallo"

#: i18n/en.yaml:0
msgid "Roadmap"
msgstr "Hoja de ruta"

#: i18n/en.yaml:0
msgid "Release History"
msgstr "Historial de lanzamientos"

#: i18n/en.yaml:0
msgid "Documentation"
msgstr "Documentación"

#: i18n/en.yaml:0
msgid "Source Code"
msgstr "Código fuente"

#: i18n/en.yaml:0
msgid "FAQ"
msgstr "Preguntas frecuentes"

#: i18n/en.yaml:0
msgid "Privacy Statement"
msgstr "Declaración sobre privacidad"

#: i18n/en.yaml:0
msgid "Tutorials"
msgstr "Tutoriales"

#: i18n/en.yaml:0
msgid "About"
msgstr "Acerca de"

#: i18n/en.yaml:0
msgid "Donations"
msgstr "Donativos"

#: i18n/en.yaml:0
msgid "Get Involved"
msgstr "Involucrarse"

#: i18n/en.yaml:0
msgid "What is KDE"
msgstr "Qué es KDE"

#: i18n/en.yaml:0
msgid "Website license"
msgstr "Licencia del sitio web"

#: i18n/en.yaml:0
msgid "Contact"
msgstr "Contacto"

#: i18n/en.yaml:0
msgid "Sitemap"
msgstr "Mapa del sitio"

#: i18n/en.yaml:0
msgid "Previous Post"
msgstr "Publicación anterior"

#: i18n/en.yaml:0
msgid "Next Post"
msgstr "Publicación siguiente"

#: i18n/en.yaml:0
msgid "{{ .ReadingTime }} minutes"
msgstr "{{ .ReadingTime }} minutos"

#: i18n/en.yaml:0
msgid "Reading time:"
msgstr "Tiempo de lectura:"

#: i18n/en.yaml:0
msgid "Could you tell us something about yourself? "
msgstr "¿Puede contarnos algo sobre usted? "

#: i18n/en.yaml:0
msgid "Download"
msgstr "Descargar"

#: i18n/en.yaml:0
msgid "Powered by"
msgstr "Funciona con"

#: i18n/en.yaml:0
msgid "Art by "
msgstr "Arte gráfico por "

#: i18n/en.yaml:0
msgid "News"
msgstr "Novedades"

#: i18n/en.yaml:0
msgid "No News"
msgstr "No hay novedades"

#: i18n/en.yaml:0
msgid "See All"
msgstr "Ver todo"

#: i18n/en.yaml:0
msgid "Tools You Need To Grow as an Artist"
msgstr "Las herramientas que necesita para crecer como artista"

#: i18n/en.yaml:0
msgid "All the features you need"
msgstr "Todas las funciones que necesita"

#: i18n/en.yaml:0
msgid ""
"Great for new and experienced artists. Here are some of the features that "
"Krita offers:"
msgstr ""

#: i18n/en.yaml:0
msgid "Multiple brush types for different art styles"
msgstr ""

#: i18n/en.yaml:0
msgid "Layers, drawing assistants, stabilizers"
msgstr "Capas, asistentes de dibujo, estabilizadores"

#: i18n/en.yaml:0
msgid "Animation tools to transform your artwork"
msgstr ""

#: i18n/en.yaml:0
msgid "Resources and Materials"
msgstr "Recursos y materiales"

#: i18n/en.yaml:0
msgid "Expand Krita’s capabilities with online tools and assets:"
msgstr ""

#: i18n/en.yaml:0
msgid "Brushes, patterns, and vector libraries"
msgstr ""

#: i18n/en.yaml:0
msgid "Texture packs and page templates"
msgstr "Paquetes de texturas y plantillas de páginas"

#: i18n/en.yaml:0
msgid "Plugins that add new functionality"
msgstr "Complementos que añaden nueva funcionalidad"

#: i18n/en.yaml:0
msgid "See Resources"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Features"
msgid "See features"
msgstr "Funcionalidades"

#: i18n/en.yaml:0
msgid "FREE education and resources"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Education"
msgid "See education"
msgstr "Educativo"

#: i18n/en.yaml:0
msgid "A supportive community"
msgstr "Una comunidad solidaria"

#: i18n/en.yaml:0
msgid ""
"An online community for Krita artists to share artwork and tips with each "
"other."
msgstr ""

#: i18n/en.yaml:0
msgid "Share your artwork and get feedback to improve"
msgstr ""

#: i18n/en.yaml:0
msgid "Ask questions about using Krita or seeing if there is a bug"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Give feedback to developers when new features and plugins are being created"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit artist community"
msgstr "Visite la comunidad de artistas"

#: i18n/en.yaml:0
msgid "Artist Interviews"
msgstr "Entrevistas de artistas"

#: i18n/en.yaml:0
msgid "Free and Open Source"
msgstr "Código abierto y libre"

#: i18n/en.yaml:0
#, fuzzy
#| msgid ""
#| "Krita is a public project licensed as GNU GPL, owned by its contributors. "
#| "For that reason Krita is Free and Open Source software, forever."
msgid ""
"Krita is a public project licensed as GNU GPL, owned by its contributors. "
"That's why Krita is Free and Open Source software, forever."
msgstr ""
"Krita es un proyecto público con licencia GPL de GNU, propiedad de sus "
"colaboradores. Por este motivo, Krita es software libre y de código abierto, "
"y siempre lo será."

#: i18n/en.yaml:0
msgid "See details about license usage"
msgstr "Consulte los detalles sobre el uso de la licencia"

#: i18n/en.yaml:0
msgid "Give back"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is made by people from all around the world – many of which are "
"volunteers. If you find Krita valuable and you want to see it improve, "
"consider becoming part of the development fund."
msgstr ""

#: i18n/en.yaml:0
msgid "Contribute to the development fund"
msgstr ""

#: i18n/en.yaml:0
msgid "Read more"
msgstr "Leer más"

#: i18n/en.yaml:0
msgid "Krita Sprint 2019 - Deventer, Netherlands"
msgstr ""

#: i18n/en.yaml:0
msgid "No trials."
msgstr "Sin versiones de prueba."

#: i18n/en.yaml:0
msgid "No subscriptions."
msgstr "Sin suscripciones."

#: i18n/en.yaml:0
msgid "No limit to your creativity."
msgstr "Sin límites para su creatividad."

#: i18n/en.yaml:0
msgid "Clean and Flexible Interface"
msgstr "Interfaz limpia y flexible"

#: i18n/en.yaml:0
msgid ""
"An intuitive user interface that stays out of your way. The dockers and "
"panels can be moved and customized for your specific workflow. Once you have "
"your setup, you can save it as your own workspace. You can also create your "
"own shortcuts for commonly used tools."
msgstr ""

#: i18n/en.yaml:0
msgid "Customizable Layout"
msgstr ""

#: i18n/en.yaml:0
msgid "Over 30 dockers for additional functionality"
msgstr "Unos 30 paneles con funcionalidad adicional"

#: i18n/en.yaml:0
msgid "Dark and light color themes"
msgstr "Temas de color oscuro y claro"

#: i18n/en.yaml:0
msgid "Learn the interface"
msgstr "Descubra la interfaz"

#: i18n/en.yaml:0
msgid "All the tools you need"
msgstr "Todas las herramientas que necesita"

#: i18n/en.yaml:0
msgid ""
"Over 100 professionally made brushes that come preloaded. These brushes give "
"a good range of effects so you can see the variety of brushes that Krita has "
"to offer."
msgstr ""

#: i18n/en.yaml:0
msgid "Beautiful Brushes"
msgstr ""

#: i18n/en.yaml:0
msgid "Learn more"
msgstr "Saber más"

#: i18n/en.yaml:0
msgid "Brush Stabilizers"
msgstr "Estabilizadores de pinceles"

#: i18n/en.yaml:0
msgid ""
"Have a shaky hand? Add a stabilizer to your brush to smoothen it out. Krita "
"includes 3 different ways to smooth and stabilize your brush strokes. There "
"is even a dedicated Dynamic Brush tool where you can add drag and mass."
msgstr ""

#: i18n/en.yaml:0
msgid "Vector & Text"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Built-in vector tools help you create comic panels. Select a word bubble "
"template from the vector library and drag it on your canvas. Change the "
"anchor points to create your own shapes and libraries. Add text to your "
"artwork as well with the text tool. Krita uses SVG to manage its vector "
"format."
msgstr ""

#: i18n/en.yaml:0
msgid "Brush Engines"
msgstr "Motores de pinceles"

#: i18n/en.yaml:0
msgid ""
"Customize your brushes with over 9 unique brush engines. Each engine has a "
"large amount of settings to customize your brush. Each brush engine is made "
"to satisfy a specific need such as the Color Smudge engine, Shape engine, "
"Particle engine, and even a filter engine. Once you are done creating your "
"brushes, you can save them and organize them with Krita's unique tagging "
"system."
msgstr ""

#: i18n/en.yaml:0
msgid "Wrap-around mode"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"It is easy to create seamless textures and patterns now. The image will make "
"references of itself along the x and y axis. Continue painting and watch all "
"of the references update instantly. No more clunky offsetting to see how "
"your image repeats itself."
msgstr ""

#: i18n/en.yaml:0
msgid "Resource Manager"
msgstr "Gestor de recursos"

#: i18n/en.yaml:0
msgid ""
"Import brush and texture packs from other artists to expand your tool set. "
"If you create some brushes that you love, share them with the world by "
"creating your own bundles. Check out the brush packs that are available in "
"the Resources area."
msgstr ""

#: i18n/en.yaml:0
msgid "Visit Resources Area"
msgstr ""

#: i18n/en.yaml:0
msgid "Simple and Powerful 2D Animation"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Turn Krita into an animation studio by switching to the animation workspace. "
"Bring your drawings to life by layering your animations, importing audio, "
"and fine tuning your frames. When you are finished, share with your friends "
"by exporting your creation to a video. Or just export the images to continue "
"working in another application."
msgstr ""

#: i18n/en.yaml:0
msgid "Features"
msgstr "Funcionalidades"

#: i18n/en.yaml:0
msgid "Multiple layers and audio support"
msgstr ""

#: i18n/en.yaml:0
msgid "Supports 1,000s of frames on timeline"
msgstr ""

#: i18n/en.yaml:0
msgid "Playback controls with pausing, playing, and timeline scrubbing"
msgstr ""

#: i18n/en.yaml:0
msgid "Onion skinning support to help with in-betweens"
msgstr ""

#: i18n/en.yaml:0
msgid "Tweening with opacity and position changes"
msgstr ""

#: i18n/en.yaml:0
msgid "Change start time, end time, and FPS"
msgstr ""

#: i18n/en.yaml:0
msgid "Export results to video or still images"
msgstr ""

#: i18n/en.yaml:0
msgid "Drag and drop frames to organize timings"
msgstr ""

#: i18n/en.yaml:0
msgid "Shortcuts for duplicating and pulling frames"
msgstr ""

#: i18n/en.yaml:0
msgid "Performance tweaking with drop-frame option"
msgstr ""

#: i18n/en.yaml:0
msgid "Productivity features"
msgstr ""

#: i18n/en.yaml:0
msgid "Drawing Assistants"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Use a drawing aid to assist you with vanishing points and straight lines. "
"The Assistant Tool comes with unique assistants to help you make that "
"perfect shape. These tools range from drawing ellipses to creating "
"curvilinear perspective with the Fisheye Point tool."
msgstr ""

#: i18n/en.yaml:0
msgid "Layer Management"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"In addition to painting, Krita comes with vector, filter, group, and file "
"layers. Combine, order, and flatten layers to help your artwork stay "
"organized."
msgstr ""

#: i18n/en.yaml:0
msgid "Select & Transform"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Highlight a portion of your drawing to work on. There are additional "
"features that allow you to add and remove from the selection. You can "
"further modify your selection by feathering and inverting it. Paint a "
"selection with the Global Selection Mask."
msgstr ""

#: i18n/en.yaml:0
msgid "Full Color Management"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita supports full color management through LCMS for ICC and OpenColor IO "
"for EXR. This allows you to incorporate Krita into your existing color "
"management pipeline. Krita comes with a wide variety of ICC working space "
"profiles for every need."
msgstr ""

#: i18n/en.yaml:0
msgid "GPU Enhanced"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"With OpenGL or Direct3D enabled, you will see increased canvas rotation and "
"zooming speed. The canvas will also look better when zoomed out. The Windows "
"version supports Direct3D 11 in place of OpenGL."
msgstr ""

#: i18n/en.yaml:0
msgid "PSD Support"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Open PSD files that even Photoshop cannot open. Load and save to PSD when "
"you need to take your artwork across different programs."
msgstr ""

#: i18n/en.yaml:0
msgid "HDR Painting"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is the only painting application that lets you open, save, edit and "
"author HDR and scene-referred images. With OCIO and OpenEXR support, you can "
"manipulate the view to examine HDR images."
msgstr ""

#: i18n/en.yaml:0
msgid "Python Scripting"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Powerful API for creating your own widgets and extending Krita. With using "
"PyQt and Krita's own API, there are many possibilities. A number of plugins "
"come pre-installed for your reference."
msgstr ""

#: i18n/en.yaml:0
msgid "Training Resources"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"In addition to training and educational material found on the Internet, "
"Krita produces its own training material to help you learn all of the tools "
"fast."
msgstr ""

#: i18n/en.yaml:0
msgid "All of this (and more) for FREE"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is, and will always be, free software. There is a lot more to learn "
"than this overview page, but you should be getting a good idea of what Krita "
"can do."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Krita"
msgstr "Descargar Krita"

#: i18n/en.yaml:0
msgid "Released on: "
msgstr "Publicado el: "

#: i18n/en.yaml:0
msgid "Release Notes"
msgstr "Notas del lanzamiento"

#: i18n/en.yaml:0
msgid "Windows Installer"
msgstr "Instalador para Windows"

#: i18n/en.yaml:0
msgid "Windows Portable"
msgstr "Portable para Windows"

#: i18n/en.yaml:0
msgid "macOS Installer"
msgstr "Instalador para macOS"

#: i18n/en.yaml:0
msgid "Linux 64-bit AppImage"
msgstr "AppImage para Linux de 64 bits"

#: i18n/en.yaml:0
msgid "Flatpak"
msgstr "Flatpak"

#: i18n/en.yaml:0
msgid "snap"
msgstr ""

#: i18n/en.yaml:0
msgid "Hosted on flathub."
msgstr ""

#: i18n/en.yaml:0
msgid "Maintained by the community."
msgstr "Mantenido por la comunidad."

#: i18n/en.yaml:0
msgid "Run"
msgstr "Ejecutar"

#: i18n/en.yaml:0
msgid "Apple iPads and iPhones are not supported"
msgstr ""

#: i18n/en.yaml:0
msgid "Google Play Store"
msgstr "Google Play Store"

#: i18n/en.yaml:0
msgid "See individual Android APK builds"
msgstr ""

#: i18n/en.yaml:0
msgid "System Requirements"
msgstr "Requisitos del sistema"

#: i18n/en.yaml:0
msgid " : "
msgstr ""

#: i18n/en.yaml:0
msgid "Operating System"
msgstr ""

#: i18n/en.yaml:0
msgid "Windows 8.1 or higher / macOS 10.12 / Linux"
msgstr ""

#: i18n/en.yaml:0
msgid "RAM"
msgstr ""

#: i18n/en.yaml:0
msgid "4 GB at minimum required. 16 GB recommended."
msgstr ""

#: i18n/en.yaml:0
msgid "GPU"
msgstr ""

#: i18n/en.yaml:0
msgid "OpenGL 3.0 or higher / Direct3D 11"
msgstr ""

#: i18n/en.yaml:0
msgid "Graphics Tablet Brand"
msgstr ""

#: i18n/en.yaml:0
msgid "Any tablet compatible with your operating system"
msgstr ""

#: i18n/en.yaml:0
msgid "Store version"
msgstr "Versión de la tienda"

#: i18n/en.yaml:0
msgid ""
"Paid versions of Krita on other platforms. You will get automatic updates "
"when new versions of Krita come out. After deduction of the Store fee, the "
"money will support Krita development. For the Microsoft store version you "
"will need Windows 10 or above."
msgstr ""

#: i18n/en.yaml:0
msgid "Microsoft Store"
msgstr "Microsoft Store"

#: i18n/en.yaml:0
msgid "Steam Store"
msgstr "Steam Store"

#: i18n/en.yaml:0
msgid "Epic Games Store"
msgstr "Epic Games Store"

#: i18n/en.yaml:0
msgid "Mac App Store"
msgstr "Mac App Store"

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Nightly Builds"
msgid "Krita Next Nightly Builds"
msgstr "Compilaciones diarias"

#: i18n/en.yaml:0
msgid ""
"New builds created daily that have bleeding-edge features. This is for "
"testing purposes only. Use at your own risk."
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Krita Next"
msgid "Visit Krita Next"
msgstr "Krita Next"

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Nightly Builds"
msgid "Krita Plus Nightly Builds"
msgstr "Compilaciones diarias"

#: i18n/en.yaml:0
msgid ""
"Contains only bug fixes on top of the currently released version of Krita"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Krita Plus"
msgid "Visit Krita Plus"
msgstr "Krita Plus"

#: i18n/en.yaml:0
msgid "Windows Shell Extension"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"The Shell extension is included with the Windows Installer. An optional add-"
"on for Windows that allow KRA thumbnails to appear in your file browser."
msgstr ""

#: i18n/en.yaml:0
msgid "Windows"
msgstr "Windows"

#: i18n/en.yaml:0
msgid "macOS"
msgstr "macOS"

#: i18n/en.yaml:0
msgid "Linux"
msgstr "Linux"

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source application. You are free to study, modify, "
"and distribute Krita under GNU GPL v3 license."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Older Versions"
msgstr "Descarga de versiones antiguas"

#: i18n/en.yaml:0
msgid ""
"If the newest version is giving you issues there are older versions "
"available for download. New versions of Krita on Windows do not support 32-"
"bit."
msgstr ""

#: i18n/en.yaml:0
msgid "GPG Signatures"
msgstr "Firmas GPG"

#: i18n/en.yaml:0
msgid ""
"Used to verify the integrity of your downloads. If you don't know what GPG "
"is you can ignore it."
msgstr ""

#: i18n/en.yaml:0
msgid "Download Signature"
msgstr "Descargar firma"

#: i18n/en.yaml:0
msgid "tarball"
msgstr "tarball"

#: i18n/en.yaml:0
msgid "Git (KDE Invent)"
msgstr "Git (KDE Invent)"

#: i18n/en.yaml:0
msgid "Old Version Library"
msgstr ""

#: i18n/en.yaml:0
msgid "Last Windows 32-bit version"
msgstr "Última versión para Windows de 32 bits"

#: i18n/en.yaml:0
msgid "Hosted on snapcraft."
msgstr ""

#: i18n/en.yaml:0
msgid "Monthly Donations"
msgstr "Donativos mensuales"

#: i18n/en.yaml:0
msgid ""
"If you join the Krita Development Fund, you will directly help keep Krita "
"getting better and better, and you will get the following:"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"A great application with powerful features, features designed together with "
"the Krita community"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Stable software where we will always try to fix issues—last year over 1,200 "
"issues were resolved!"
msgstr ""

#: i18n/en.yaml:0
msgid "Visibility and recognition online, via community badges"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit Development Fund"
msgstr ""

#: i18n/en.yaml:0
msgid "One-time Donation"
msgstr "Donativos únicos"

#: i18n/en.yaml:0
msgid ""
"One-time donations will take you to the Mollie payment portal. Donations are "
"done in euros as that provides the most payment options."
msgstr ""

#: i18n/en.yaml:0
msgid "Accepted payment methods:"
msgstr "Métodos de pago aceptados:"

#: i18n/en.yaml:0
msgid "Transaction ID: "
msgstr "ID de la transacción: "

#: i18n/en.yaml:0
msgid "Paid €%s"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"If there are issues with your donation, you can share this transaction ID "
"with us to help us know exactly which donation was yours."
msgstr ""

#: i18n/en.yaml:0
msgid "Back to website"
msgstr "Volver al sitio web"

#: i18n/en.yaml:0
msgid "Have Fun Painting"
msgstr ""

#: i18n/en.yaml:0
msgid "Having trouble downloading?"
msgstr ""

#: i18n/en.yaml:0
msgid "Visit the Downloads area"
msgstr ""

#: i18n/en.yaml:0
msgid "What's Next"
msgstr ""

#: i18n/en.yaml:0
#, fuzzy
#| msgid "Maintained by the community."
msgid "Connect with the Community"
msgstr "Mantenido por la comunidad."

#: i18n/en.yaml:0
msgid "Share artwork and ask questions on the forum."
msgstr ""

#: i18n/en.yaml:0
msgid "Krita Artists forum"
msgstr "Foro de artistas de Krita"

#: i18n/en.yaml:0
msgid "Watch videos to learn about new brushes and techniques."
msgstr ""

#: i18n/en.yaml:0
msgid "Krita YouTube channel"
msgstr "Canal de YouTube de Krita"

#: i18n/en.yaml:0
msgid "Free Learning Resources"
msgstr "Recursos de aprendizaje libre"

#: i18n/en.yaml:0
msgid "Getting Started"
msgstr "Primeros pasos"

#: i18n/en.yaml:0
msgid "User Interface"
msgstr "Interfaz del usuario"

#: i18n/en.yaml:0
msgid "Basic Concepts"
msgstr "Conceptos básicos"

#: i18n/en.yaml:0
msgid "Animations"
msgstr "Animaciones"

#: i18n/en.yaml:0
msgid "User Manual"
msgstr "Manual del usuario"

#: i18n/en.yaml:0
msgid "Layers and Masks"
msgstr "Capas y máscaras"

#: i18n/en.yaml:0
msgid "Like what we are doing? Help support us"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"Krita is a free and open source project. Please consider supporting the "
"project with donations or by buying training videos or the artbook! With "
"your support, we can keep the core team working on Krita full-time."
msgstr ""

#: i18n/en.yaml:0
msgid "Buy something"
msgstr ""

#: i18n/en.yaml:0
msgid ""
"There should have been a video here but your browser does not seem to "
"support it."
msgstr ""

#: i18n/en.yaml:0
msgid "Nope. Not in here either... (404)"
msgstr "No, aquí tampoco... (404)"

#: i18n/en.yaml:0
msgid ""
"Either the page does not exist any more, or the web address was typed wrong."
msgstr ""
"La página ya no existe o la dirección web que ha escrito es incorrecta."

#: i18n/en.yaml:0
msgid "Go back to the home page"
msgstr "Volver a la página web"

#~ msgid "Gentoo"
#~ msgstr "Gentoo"

#~ msgid "KDE Repository"
#~ msgstr "Repositorio de KDE"

#~ msgid "Shop"
#~ msgstr "Tienda"
