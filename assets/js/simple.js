// create a library of images and artists to draw from
// make sure we give them credit and a place for people to check out their stuff
var randomImageLibrary = [
    {
        imageUrl: 'images/pages/ngan-pham-girl-releashing-fish-4-resize.webp',
        artist: 'Ngan Pham',
        portfolioUrl: 'https://www.artstation.com/zevania'
    },
    {
        imageUrl: 'images/pages/albert-weand-adventurers.webp',
        artist: 'Albert Weand',
        portfolioUrl: 'https://www.artstation.com/aweand'
    },
    {
        imageUrl: 'images/pages/serg-grafik-blue-green-landscape.webp',
        artist: 'Serg Grafik',
        portfolioUrl: 'https://www.artstation.com/serggrafik'
    },
    {
        imageUrl: 'images/pages/revoy-plxthumbnailer.webp',
        artist: 'David Revoy',
        portfolioUrl: 'https://www.davidrevoy.com/'
    },
    {
        imageUrl: 'images/pages/ravioli.webp',
        artist: 'RavioliMavioli',
        portfolioUrl: 'https://krita-artists.org/u/raviolimavioli/activity/portfolio'
    },
];

// pick random image from library to use on page
var randomImageIndex = Math.floor(Math.random()* randomImageLibrary.length );

// update hero element with new image and to be credit
var currentStyles = document.getElementById("slim-hero").getAttribute("style");
document.getElementById("slim-hero")
.setAttribute("style", `background: url(${ randomImageLibrary[randomImageIndex].imageUrl  });` +
'background-size: cover;  background-position: center top;'
);

// update the credits section
var creditLinkEl = document.getElementById("credit-link");
creditLinkEl.setAttribute("href", randomImageLibrary[randomImageIndex].portfolioUrl);
creditLinkEl.insertBefore(document.createTextNode(randomImageLibrary[randomImageIndex].artist), creditLinkEl.firstChild);
